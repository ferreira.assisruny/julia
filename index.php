<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		include "head.php";
	?>
</head>
<body>

	<main class="container py-4 text-center">
		
		<br>
		<h1>
			<img src="assets/img/african-woman.svg" alt="african-woman" height="84">
			Julia | Chatbot Engine
		</h1>

		<br>
		<p>
		A productive chatbot development engine,<br>
		where we can build chatbots in simple<br>
		way without spend time.
		</p>
		
		<?php
			include "nav.php";
		?>
		
	</main>
	
</body>
</html>